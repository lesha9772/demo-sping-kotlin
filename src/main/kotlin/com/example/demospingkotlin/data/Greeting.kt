package com.example.demospingkotlin.data

data class Greeting(val id: Long, val content: String)