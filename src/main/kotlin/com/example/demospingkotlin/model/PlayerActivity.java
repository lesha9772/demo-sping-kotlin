package com.example.demospingkotlin.model;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "player_activity")
public class PlayerActivity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Integer id;

    @CreationTimestamp
    private LocalDateTime created;

    @Getter
    @UpdateTimestamp
    @Column(name = "event_date")
    private LocalDateTime eventDate;

    @Setter
    @Getter
    @Column(name = "player_token")
    private String playerToken;

    public PlayerActivity() {
    }

    public PlayerActivity(String playerToken) {
        this.created = LocalDateTime.now();
        this.playerToken = playerToken;
    }

    public void updateEventDate() {
        this.eventDate = LocalDateTime.now();
    }
}
