package com.example.demospingkotlin.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author Maks Plys
 *
 */
@Entity
@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "incident_logs")
public class IncidentLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int incidentId;

    @Column(name = "created", nullable = false)
    @CreationTimestamp
    private LocalDateTime created;

    @Column(name = "player_token", nullable = false)
    private String playerToken;

    @Column(name = "error_type")
    private String errorType;

    @Column(name = "endpoint_path")
    private String endpointPath;

    @Column(name = "exception_source")
    private String exceptionSource;

    @Column(name = "exception_type")
    private String exceptionType;

    @Column(name = "exception_message", length = 1024)
    private String exceptionMessage;

    @Column(name = "stack_trace", columnDefinition = "TEXT")
    private String stackTrace;
}

