package com.example.demospingkotlin.repository

import com.example.demospingkotlin.model.ParkRunner
import com.example.demospingkotlin.model.PlayerActivity
import org.springframework.data.repository.CrudRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = "player", path = "player")
interface PlayerActivityRepository : CrudRepository<PlayerActivity, Long> {
}