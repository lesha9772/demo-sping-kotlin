package com.example.demospingkotlin.repository

import com.example.demospingkotlin.model.ParkRunner
import org.springframework.data.repository.CrudRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = "runners", path = "runners")
interface ParkRunnerRepository : CrudRepository<ParkRunner, Long> {
}