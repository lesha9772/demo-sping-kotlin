package com.example.demospingkotlin

import com.example.demospingkotlin.model.ParkRunner
import com.example.demospingkotlin.repository.ParkRunnerRepository
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean


import org.springframework.context.support.beans
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.web.servlet.invoke
import org.springframework.security.core.userdetails.User
import org.springframework.security.provisioning.InMemoryUserDetailsManager
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.router
import java.lang.Compiler.disable

@SpringBootApplication
class DemoSpingKotlinApplication{
	@Bean
	fun run(repository : ParkRunnerRepository) = ApplicationRunner {

		repository.save(
			ParkRunner(firstName = "NEERAJ", lastName="SIDHAYE", gender="M",
			totalRuns="170", runningClub="RUNWAY")
		)
	}
}

@EnableWebSecurity
class KotlinSecurityConfiguration : WebSecurityConfigurerAdapter() {

	override fun configure(http: HttpSecurity?) {
		http {
			csrf { disable() }
			formLogin { disable() }
			httpBasic {}
			authorizeRequests {
				authorize("/greetings/**", hasAuthority("ROLE_ADMIN"))
				authorize("/h2-console/**", permitAll)
				authorize("/runners/**", permitAll)
				authorize("/runners", permitAll)
				authorize("/v1/joystik/**", permitAll)
				authorize("/**", permitAll)
			}
		}
	}
}

fun main(args: Array<String>) {
	runApplication<DemoSpingKotlinApplication>(*args) {
		addInitializers(beans {
			bean {

				fun user(user: String, pw: String, vararg roles: String) =
					User.withDefaultPasswordEncoder().username(user).password(pw).roles(*roles).build()

				InMemoryUserDetailsManager(user("jlong", "pw", "USER"), user("rwinch", "pw1", "USER", "ADMIN"))
			}
			bean {
				router {
					GET("/greetings") { request ->
						request.principal().map { it.name }.map { ServerResponse.ok().body(mapOf("greeting" to "Hello, $it")) }.orElseGet { ServerResponse.badRequest().build() }
					}
				}
			}
		})
	}
}
