package com.example.demospingkotlin.controller


import com.example.demospingkotlin.data.Greeting
import org.springframework.web.bind.annotation.*
import java.util.concurrent.atomic.AtomicLong

@RestController
class JoystikController {

    val counter = AtomicLong()

    @PostMapping("/v1/joystik/login")
    fun loginByCode(@RequestParam(value = "code", defaultValue = "12345") code: String) =
            Greeting(counter.incrementAndGet(), "loginByCode, $code")

    @PostMapping("/v1/joystik/alive")
    fun alive(@RequestParam(value = "code", defaultValue = "12345") code: String) =
            Greeting(counter.incrementAndGet(), "Alive, $code")

    @GetMapping("/v1/joystik/state")
    fun state(@RequestParam(value = "code", defaultValue = "12345") code: String) =
            Greeting(counter.incrementAndGet(), "Todo - return State code, $code")

    @GetMapping("/v1/joystik/event")
    fun getEvent(@RequestParam(value = "code", defaultValue = "12345") code: String) =
            Greeting(counter.incrementAndGet(), "return Event info, title category, payer name, $code")

    @PutMapping("/v1/joystik/save-score")
    fun saveScore(@RequestParam(value = "code", defaultValue = "12345") code: String) =
            Greeting(counter.incrementAndGet(), "Save score info for event, $code")

    @GetMapping("/v1/joystik/event-result")
    fun getEventResult(@RequestParam(value = "code", defaultValue = "12345") code: String) =
            Greeting(counter.incrementAndGet(), "Get event result with score all judges, $code")

}