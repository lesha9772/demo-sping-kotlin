package com.example.demospingkotlin.controller


import com.example.demospingkotlin.data.Greeting
import org.springframework.web.bind.annotation.*
import java.util.concurrent.atomic.AtomicLong

@RestController
class TvController {

    val counter = AtomicLong()

    @PostMapping("/v1/tv/login")
    fun loginByCode(@RequestParam(value = "code", defaultValue = "12345") code: String) =
            Greeting(counter.incrementAndGet(), "loginByCode, $code")

    @PostMapping("/v1/tv/alive")
    fun alive(@RequestParam(value = "code", defaultValue = "12345") code: String) =
            Greeting(counter.incrementAndGet(), "Alive, $code")

    @GetMapping("/v1/tv/state")
    fun state(@RequestParam(value = "code", defaultValue = "12345") code: String) =
            Greeting(counter.incrementAndGet(), "Todo - return State code, $code")

    @GetMapping("/v1/tv/event")
    fun getEvent(@RequestParam(value = "code", defaultValue = "12345") code: String) =
            Greeting(counter.incrementAndGet(), "return Event info, title category, payer name, $code")

}