package com.example.demospingkotlin.controller


import com.example.demospingkotlin.data.Greeting
import org.springframework.web.bind.annotation.*
import java.util.concurrent.atomic.AtomicLong

@RestController
class SecretaryController {

    val counter = AtomicLong()

    @PostMapping("/v1/secretary/login")
    fun loginByCode(@RequestParam(value = "code", defaultValue = "12345") code: String) =
            Greeting(counter.incrementAndGet(), "loginByCode, $code")

    @PostMapping("/v1/secretary/alive")
    fun alive(@RequestParam(value = "code", defaultValue = "12345") code: String) =
            Greeting(counter.incrementAndGet(), "Alive, $code")

    @GetMapping("/v1/secretary/events")
    fun events(@RequestParam(value = "code", defaultValue = "12345") code: String) =
            Greeting(counter.incrementAndGet(), "events, $code")

    @GetMapping("/v1/secretary/event-info")
    fun eventInfo(@RequestParam(value = "code", defaultValue = "12345") code: String) =
            Greeting(counter.incrementAndGet(), "event-info, $code")

    @PostMapping("/v1/secretary/set-event")
    fun setEvent(@RequestParam(value = "code", defaultValue = "12345") code: String) =
            Greeting(counter.incrementAndGet(), "set-event, $code")

    @PostMapping("/v1/secretary/set-score")
    fun setScore(@RequestParam(value = "code", defaultValue = "12345") code: String) =
            Greeting(counter.incrementAndGet(), "set-score, $code")

    @PostMapping("/v1/secretary/start-event")
    fun startEvent(@RequestParam(value = "code", defaultValue = "12345") code: String) =
            Greeting(counter.incrementAndGet(), "start event, $code")


}